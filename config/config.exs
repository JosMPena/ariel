# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :ariel,
  ecto_repos: [Ariel.Repo]

# Configures the endpoint
config :ariel, Ariel.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "DLlFO1ZrXPYet3qkxAeSDv7BKMU7R3b9i8qeKAMJakiUUOotieekfcnKZSUpPZ6i",
  render_errors: [view: Ariel.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Ariel.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# use json_library for phoenix 1.4
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
