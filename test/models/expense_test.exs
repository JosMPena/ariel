defmodule Ariel.ExpenseTest do
  use Ariel.ModelCase

  alias Ariel.Expense

  @valid_attrs %{amount: "120.5", category: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Expense.changeset(%Expense{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Expense.changeset(%Expense{}, @invalid_attrs)
    refute changeset.valid?
  end
end
