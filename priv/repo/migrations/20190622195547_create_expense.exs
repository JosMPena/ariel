defmodule Ariel.Repo.Migrations.CreateExpense do
  use Ecto.Migration

  def change do
    create table(:expenses) do
      add :amount, :float
      add :currency, :string
      add :category, :string
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end
    create index(:expenses, [:user_id])

  end
end
