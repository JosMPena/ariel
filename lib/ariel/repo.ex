defmodule Ariel.Repo do
  use Ecto.Repo, otp_app: :ariel, adapter: Ecto.Adapters.Postgres
end
