defmodule Ariel.FeedView do
  use Ariel.Web, :view

  def render("index.json", %{feeds: feeds}) do
    %{data: render_many(feeds, Ariel.FeedView, "feed.json")}
  end

  def render("show.json", %{feed: feed}) do
    %{data: render_one(feed, Ariel.FeedView, "feed.json")}
  end

  def render("feed.json", %{feed: feed}) do
    feed
  end
end
