defmodule Ariel.User do
  use Ariel.Web, :model

  schema "users" do
    field :uid, :string
    has_many :expenses, Ariel.Expense

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:uid])
    |> validate_required([:uid])
  end
end
