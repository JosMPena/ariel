defmodule Ariel.Expense do
  use Ariel.Web, :model

  schema "expenses" do
    field :amount, :float
    field :currency, :string
    field :category, :string
    belongs_to :user, Ariel.User

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:amount, :category])
    |> validate_required([:amount, :category])
  end
end
