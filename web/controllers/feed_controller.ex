defmodule Ariel.FeedController do
  use Ariel.Web, :controller
  require Logger

  def index(conn, params) do
    confidence = get_in(params, ["queryResult", "intentDetectionConfidence"])
    if confidence >= 0.6 do
      user = find_or_create_user(params)
      if user do
        changeset = user
                     |> build_assoc(:expenses)
                     |> Ariel.Expense.changeset(expense_params(params))
        case Repo.insert(changeset) do
          {:error, changeset} ->
            Logger.debug changeset.errors
        end
      end
    end

    conn
      |> render("index.json")
  end

  def create(conn, %{"feed" => feed_params}) do
    #    changeset = Feed.changeset(%Feed{}, feed_params)
    #
    #    case Repo.insert(changeset) do
    #      {:ok, feed} ->
    #        conn
    #        |> put_status(:created)
    #        |> put_resp_header("location", feed_path(conn, :show, feed))
    #        |> render("show.json", feed: feed)
    #      {:error, changeset} ->
    #        conn
    #        |> put_status(:unprocessable_entity)
    #        |> render(Ariel.ChangesetView, "error.json", changeset: changeset)
    #    end
  end

  def show(conn, params) do
    feed = parse_feed(params)
    render(conn, "show.json", feed: feed)
  end

  def update(conn, %{"id" => id, "feed" => feed_params}) do
    #    feed = Repo.get!(Feed, id)
    #    changeset = Feed.changeset(feed, feed_params)
    #
    #    case Repo.update(changeset) do
    #      {:ok, feed} ->
    #        render(conn, "show.json", feed: feed)
    #      {:error, changeset} ->
    #        conn
    #        |> put_status(:unprocessable_entity)
    #        |> render(Ariel.ChangesetView, "error.json", changeset: changeset)
    #    end
  end

  def delete(conn, %{"id" => id}) do
    feed = Repo.get!(Feed, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(feed)

    send_resp(conn, :no_content, "")
  end

  defp parse_feed(params) do
    params
  end

  def find_or_create_user(params) do
    user_id = params
              |> get_user_id()
    case (Repo.get_by(Ariel.User, uid: user_id) || create_user(%{uid: user_id})) do
      user = %Ariel.User{} ->
        user
      changeset ->
        nil
    end
  end

  def find_category(params) do
    fulfillment = params
                  |> get_in(["queryResult", "fulfillmentText"])
    Regex.scan(~r/\bcategoría\s*\K\w+/, fulfillment)
      |> List.flatten()
      |> List.last()
  end

  def get_user_id(params) do
    from = get_in(params, ["originalDetectIntentRequest", "payload", "data", "message", "from"])
    case from do
      %{"username" => username, "id" => id} ->
        username <> "_" <> Float.to_string(id)
      nil ->
        String.split(params["session"], "/")
        |> List.last()
      _ ->
        nil
    end
  end

  def create_user(user_params) do
    changeset = Ariel.User.changeset(%Ariel.User{}, user_params)

    case Repo.insert(changeset) do
      {:ok, user} ->
        user
      {:error, changeset} ->
        changeset
    end
  end

  def get_amount(params) do
    params
      |> get_in(["queryResult", "parameters", "number"])
  end

  def expense_params(params) do
    category = find_category(params)
    amount = get_amount(params)
    %{amount: amount, category: category}
  end
end
